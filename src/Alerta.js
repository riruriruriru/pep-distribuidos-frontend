import React, { Component } from 'react';
import logo from './logos/logo28.png';
import './Header.css';
import { Alert, Container} from "react-bootstrap";
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import regiones from './urls.js'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class Alerta extends React.Component {
    constructor(props) {
    super(props);
        //this.handleLogin = this.handleLogin.bind(this);

    }
    
     render() {
      return (
        

          <Container >
         <Alert className="alerta" variant="success">
          <Alert.Heading><b>ATENCIÓN</b></Alert.Heading>
          <ul>
    <li>Debe tener en consideración que la duración de este Permiso Temporal tendrá una validez de 03 horas.</li>
    <li>No será válido en horario de toque de queda </li>
          </ul>
<p> <b>ADVERTENCIA</b>: Cualquier persona que se encuentre bajo alguna restricción sanitaria que implique aislamiento o cuarentena, por ser paciente confirmado con diagnóstico <b>COVID19, EN ESPERA DE RESULTADOS DE EXÁMENES COVID19 Y/O PROVENIENTES DEL EXTRANJERO</b>, no puede solicitar y/o tramitar Salvoconducto y/o Permiso Temporal alguno.</p>
        </Alert>
          </Container>
         
          
      );
    }
}