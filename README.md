# Wiki Pep 1 Sistemas Distribuidos:

## Definición Sistema Distribuido:
- "Un sistema distribuido es un sistema en el cual los componentes de hardware o software localizados en una red de computadores pueden comunicarse y coordinar sus acciones mediante el paso de mensajes"
- "Una colección de computadores independientes que para el usuario parecen ser un sólo sistema coherente"
## Análisis de un Sistema Distribuido:
Un sistema distribuido debe cumplir ciertas características básicas o características deseables.
### Características de un Sistema Distribuido:
#### Transparencia: 
La transparencia debe esconder el hecho de que procesos y recursos están distribuidos en múltiples computadores, por lo tanto para el usuario o aplicaciones debe parecer un sistema único. La transparencia se relaciona con el acceso, ubicación, migración, re-localización, replicación, concurrencia y fallas. Además ayuda a lidiar con la heterogeneidad del hardware.
#### Escalabilidad:
Puede lograrse de forma horizontal (aumentar cantidad de recursos) o vertical (aumentar capacidad de recurso). Además esto se puede lograr mediante el particionar (dividir y distribuir), replicar (copiar en otro lugar) y cache (recursos de acceso fácil y rápido)
#### Disponibilidad:
El sistema construido debe ser confiable, debido a que tanto hardware como software pueden fallar. Una forma de lograr disponibilidad es mediante la réplica de recursos.
#### Apertura:
El sistema distribuido abierto debe ofrecer servicios bajo reglas estándar de sintaxis o semántica. Debe permitir interoperabilidad, portabilidad y facil extensión.
## Descripción Sistema Legado
El sistema legado para obtener permisos fue desarrollado con una arquitectura simple de Cliente-Servidor-Base de datos.
### Servidor:
Para el servidor, se utilizó el framework de backend Express, basado en el lenguaje NodeJS. El objetivo principal de Express es ser un "framework de desarrollo de aplicaciones web minimalista y flexible para Node.js". Las características principales de Node.js y Express son las siguientes:
##### Node.js
- Gran rendimiento y escalabilidad en aplicaciones web en tiempo real.
- Lenguaje JavaScript, el cual es simple de utilizar y ofrece mejores características para el desarrollo web que PHP o Python.
- Gestor de paquetes NPM que facilita y proporciona el acceso a miles de paquetes.
- Portable: Versiones disponibles para Windows, OS X, Linux, Solaris, WebOS, etc.
##### Express
- Manejo de peticiones con diferentes verbos HTTP y diferentes URL o rutas.
- Permite establecer el puerto para conectar, procesamiento de peticiones "middleware", librerías para trabajar con cookies, sesiones, parámetros URL, datos POST, etc.
#### Descripción Servidor:
El servidor se utiliza para implementar una API REST, que mediante HTTP realiza operaciones PUT y GET para ingresar y obtener permisos desde una base de datos. Además, se utiliza la librería "sequelize" para abstraer el uso de la base de datos e implementar una ORM (object relational database), pudiendo crear un simple modelo (que se traduce a una tabla en la base de datos) y un controlador que permite realizar las operaciones GET o POST.
### Cliente:
La parte del cliente o frontend se desarrolló utilizando el framework ReactJS, el cual también está basado en JavaScript, entre sus principales características se encuentran:
- Desarrollo de aplicaciones web ordenado y menos verboso.
- Permite asociar vistas con los datos, para cuando se cambie o modifique un dato, también se actualice la vista.
- Manejo de componentes que permiten encapsular funcionamiento y presentación.
- Gracias al uso de Virtual DOM, cuando se actualiza una vista, React actualiza el DOM virtual en vez del real, lo cual es más rápido. 
- Por lo tanto, cuando React compara el Dom virtual con el real, sabe qué partes específicas debe actualizar y se ahorra el actualizar la vista completa. Esto se hace de forma transparente para el desarrollador.
- Gracias a estar basado en componentes, estos se pueden reutilizar en el mismo proyecto o en otros proyectos.
#### Descripción Cliente:
Para utilizar las ventajas de react, se crearon varios componentes distintos para representar el header, footer y el formulario en sí. Mediante la librería Axios se realizan las request al servidor. También se utiliza la api "División Político Administrativa" para obtener y mostrar las regiones de Chile y sus comunas.
### Base de datos:
La base de datos utilizada es postgresql, que corresponde a un open-source RDBMS, lo que significa que es un Sistema Gestor de Bases de Datos Relacionales cuyos focos principales son la Extensibilidad(facultad de flexibilidad para el cambio como soportar nuevas funcionalidades) y el Cumplimiento de Estándares. Las caracteristcias principales de postrgresql son las siguientes:
##### Postgresql
- Control de Concurrencia Multi-Versión, lo que permite mantener "instantaneas" o bien versiones de los datos, lo que impide que las instrucciones vean inconsistencia en los datos producidas por transacciones concurrentes que realizan actualizaciones en las mismas filas de datos
- La Recuperación point-in-time ofrece a PostgreSQL la habilidad de recuperar datos de una forma simple, rápida y de forma íntegra, restaurando el estado de la base de datos a un punto anterior en el tiempo
- Facilidad y agilidez en la manipulacion de objetos, gracias a su integracion con paquetes compatibles con Node.js con la ayuda de Sequelize ORM, compatible ademas con otros gestores como MySQL, SQLite y MSSQL.
## Analisis Sistema Legado
El siguiente analisis considera las carectersiscticas implementadas en el sistema legado, y las caractersiticas deseables que debe considerar un sistema distribuido
### Transparencia
- **Acceso**: Gracias a la utilizacion del gestor de paquetes de Node.js (npm), tanto local como remotamente, basta con utilizar los comandos respectivos para instalar cada uno de los modulos que requiere el sistema para funcionar y también para ponerlo en ejecución, mas aún, gracias a la capa de abstraccion proporcionada por los contenedores de docker, independiente del sistema operativo en el que se encuentre, se tiene el mismo acceso con los mismos comandos para "buildear" el contenedor y levantarlo.

- **Ubicación**: El sistema no con cuenta con un dominio proporcionado por el protocolo DNS, por lo que practicamente el cliente trabaja directamente con la dirección IP del servidor en el que se esta ejecutando la aplicación.

- **Migración**: De la mano con lo que es el acceso, y gracias al complemento tanto del gestor de paquetes de Node.js "npm", y a los contenedores de docker, la migracion se hace bastante sencilla ya que basta con realizar un par de comandos sobre el contenedor de docker que contiene el sistema, para buildear y levantar este independiente del sistema operativo del servidor.

- **Re-localización**: De mano con la Ubicación, al no tener un sistema de nombres, re-localizar el sistema a otro servidor supone trabajar con una nueva direccion IP y por tanto dar a conocer al usuario que el sistema esta siendo movido a otro lugar y posiblemente dejar inhabilitada la direccion anterior para ser utilizada normalmente.

- **Replicación**: El sistema no cuenta con replicación pues consta solamente de una base de datos y un servidor que corre el backend y frontend. Sin embargo, si existiera, y gracias a la arquitectura Cliente-Servidor-BD y al implementar el sistema como una API-REST, la replicacion se asegura transparente. Gracias a docker, esto se podría realizar independientemente del Sistema Operativo.

- **Concurrencia**: A traves del objeto "Request" proporcionado por el framework de express, la concurrencia de solicitudes se da de forma transparente, sin evidenciar a los usuarios que estan accediendo a un mismo recurso (backend y base de datos).

- **Falla**: En cuanto a transparencia de fallas, no lo es un su totalidad, pero dependiedo del tipo de falla esta podria denotarse de manera transparente o no, por ejemplo alguna falla a nivel de back-end no afecta en ninguna medida al correcto despliegue del front-end, de igual manera si fallara la base de datos.

### Escalabilidad
- **Vertical**: Gracias a la implementacion de contenedores de software para montar la API con docker, la escalabilidad vertical se presenta de manera mucho mas sencilla, ya que esta capa de abstraccion dota al sistema de una versatilidad a la hora de migrar o cambiar el servidor por uno de mas capacidad, independiente de la plataforma sobre la cual se desempeñe este nuevo servidor. De igual manera, tambien se encuentra disponible la escalabilidad vertical considerando solamente modificaciones de los recursos ya disponibles en el servidor actual que potencie sus capacidades.

- **Horizontal**: Si bien no hay escalabilidad horizontal, el hecho de utilizar contenedores facilita el posible escalamiento vertical del sistema simplemente agregando nuevos nodos o servidores, para equilibrar mejor la carga del sistema, independiente del sistema operativo en cual se desenvuelvan estos nuevos nodos. En este sentido, los nuevos nodos solamente necesitaran tener instalado docker y a traves de los comandos "build" y "up" se montara el sistema en un contenedor disponible para su despliegue. De forma paralela, se puede trabajar la escalabildad por el lado de la base de datos, replicando la ya existente hacia otras tales como Apache Cassandra o Apache HBase, lo que se considera para futuras modificaciones, facilitado ademas por los paquetes ofrecidos por Node.js con los ORM de Cassandra y HBase, u alguna otra base de datos distribuida. Por lo tanto, aunque no hay escalabilidad horizontal, solo agregando el uso de algunas tecnologías (como Kubernetes, otras bases de datos), podría lograrse utilizando los mismos contenedores de backend y front end.

### Disponibilidad
Actualmente el sistema no cuenta con una disponibilidad altamente tolerante a fallos, a pesar de que testeandolo alcanza a soportar un promedio de 3000 usuarios en un minuto o quizas un poco mas sin incurrir en fallas, ante una eventual no cuenta con mecanismos para manejarlos, tales como replicas de nodos o la base de datos, por lo tanto tampoco est disponible la opcion de balancear la carga en caso de que algun servidor o la misma base de datos falle, aun asi tomando en cuenta la horizontalidad que si esta disponible en este sistema, facilmente se pueden agregar nuevos servidores y bases de datos como ya se menciona, para mejorar la capacidad de maniobra del sistema ante posible fallas. Considerando ademas que la base de datos esta implementada en base a PostgreSQL, este ultimo prtmite manejar la base de datos arquitecturalmente como una relacion Maestro-Esclavo, por lo que se puede destinar una base de datos maestra, apoyada por uno o mas servidores de respaldo listos para tomar accion en caso de que el servidor primario falle, ahora bien, esta implementacion se proyecta para futuras modificaciones.

### Apertura
La apertura del sistema esta presente desde el momento en que este esta implementado en forma de una API REST, que le permite se utilizada por otro software como capa de abstracción. Es Portable, gracias al uso de contenedores de docker facilita el despliegue del sistema en multiples sistemas operativos. Es de facil extension, considerando que componentes nuevos pueden ser agregados gracias al sistema gestor de paquetes de Node.js, ubicados en el repositorio npm. Los servicios proporiconados por el sistema se rigen por reglas estandar utilizando el protocolo HTTP para llevar a cabo operaciones GET y PUT sobre la base de datos.

## Test
Para analizar la capacidad de respuesta, se utilizó Artillery.io, con el cual se simularon request POST para el backend de la aplicación.
Se generó un archivo .yml con un test el cual tiene las siguientes características:
- **Duración**: 60 segundos
- **Tasa de llegada**: 50 usuarios por segundo.
- **Usuarios totales**: El test se divide en 6 etapas de 10 segundos, por lo tanto se tiene un total de 3000 usuarios virtuales en un minuto.
- **Request**: Todos los usuarios hacen un request POST en el cual generan un permiso nuevo.

### Resultados
Se obtuvieron los siguientes resultados:



![alt text](testBackPost.png "Test Back: Solicitudes POST")

De las 3000 solicitudes, se pueden desprender los siguientes datos:
- Las 3000 solicitudes fueron exitosas, debido a que se recibieron 3000 respuestas "200"
- Se completaron los 3000 escenarios y las 3000 solicitudes satisfactoriamente.
- Se tuvo un tiempo de respuesta mínimo de 290 microsegundos y un promedio de 306 microsegundos, el máximo fue de 341 microsegundos, por lo tanto el tiempo de respuesta de la aplicación fue consistente.

## Página:
Para conectarse a la aplicación, hay que ingresar a la dirección: "http://35.224.190.143:3000/".
En la cual se despliega una vista con un formulario que permite pedir un permiso.